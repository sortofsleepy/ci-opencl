# CI-OpenCL #

A port of [ofxMSAOpenCL](https://github.com/memo/ofxMSAOpenCL) primarily done as an exercise. 
Apart from some minor alterations to cope with glNext and make things more generic sounding, it's more or less the same apart from 
image integration which I haven't gotten around to yet.

`WARNING` bugs are likely :smile:


### How do I get set up? ###

* no cinder block yet so dump all the files in the `xcode/openCL` folder into your project
* `NOTE` mac only at the moment.

## Many thanks to 

* [ofxMSAOpenCL](https://github.com/memo/ofxMSAOpenCL)
* [Memo Akten](http://www.memo.tv/category/work/by-type/)