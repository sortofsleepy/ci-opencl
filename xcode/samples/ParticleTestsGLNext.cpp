#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include "cinder/Log.h"
#include "MSAOpenCL.h"
using namespace ci;
using namespace ci::app;
using namespace std;
typedef vec2 float2;
#include "cinder/Rand.h"
#define NUM_PARTICLES (1000000)


class CinderOpenCLApp : public App {
public:
    void setup() override;
    void mouseMove(MouseEvent event) override;
    void mouseDown( MouseEvent event ) override;
    void update() override;
    void draw() override;
    ci::gl::VboRef vbo;
    gl::VaoRef vao;
    
    typedef struct{
        float2 vel;
        float mass;
        float dummy;		// need this to make sure the float2 vel is aligned to a 16 byte boundary
    } Particle;
    
    
    float2				mousePos;
    float2				dimensions;
    
    
    OpenCL			opencl;
    
    OpenCLBufferManagedT<Particle>	particles; // vector of Particles on host and corresponding clBuffer on device
    
    float2				particlesPos[NUM_PARTICLES];
    OpenCLBufferManagedT<float2> particlePos; // vector of particle positions on host and corresponding clBuffer, and vbo on device
    
    
    
};

void CinderOpenCLApp::setup()
{
    
    
    vao = gl::Vao::create();
    
    opencl.setupFromOpenGL();
    
    vbo = gl::Vbo::create(GL_ARRAY_BUFFER);
    
    vao->bind();
    gl::ScopedBuffer buff(vbo);
    vbo->bufferData(sizeof(float2) * NUM_PARTICLES, 0, GL_DYNAMIC_COPY );
    gl::enableVertexAttribArray(0);
    gl::vertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE,0,0);
    vao->unbind();
    
    particles.initBuffer(NUM_PARTICLES);
    particlePos.initFromGLObject(vbo->getId(), NUM_PARTICLES);
    
    
    for(int i=0; i<NUM_PARTICLES; i++) {
        Particle &p = particles[i];
        p.vel = vec2(0, 0);
        p.mass = randFloat(0.5, 1);
        
        auto randWidth = randFloat(getWindowWidth());
        auto randHeight = randFloat(getWindowHeight());
        
        particlePos[i] = vec2(randWidth,randHeight);
        
    }
    
    
    particles.writeToDevice();
    particlePos.writeToDevice(); ///< uploads buffer data to shared CL/GL memory, so the vbo and the cl buffer are written in one go, since they occupy the same memory locations.
    
    
    opencl.loadProgramFromFile("Particle.cl");
    opencl.loadKernel("updateParticle");
    
    opencl.kernel("updateParticle")->setArg(0, particles);
    opencl.kernel("updateParticle")->setArg(1, particlePos);
    opencl.kernel("updateParticle")->setArg(2, mousePos);//.getPtr(), sizeof(float2));
    opencl.kernel("updateParticle")->setArg(3, dimensions);//.getPtr(), sizeof(float2));
    
    gl::pointSize(10);
    
    dimensions = vec2(getWindowWidth(),getWindowHeight());
    
    
    
    gl::enable(GL_PROGRAM_POINT_SIZE);
    gl::pointSize(1.0);
    
    
}
void CinderOpenCLApp::mouseMove(cinder::app::MouseEvent event)
{
    
    mousePos = event.getPos();
}
void CinderOpenCLApp::mouseDown( MouseEvent event )
{
}

void CinderOpenCLApp::update()
{
    
    opencl.kernel("updateParticle")->setArg(2, mousePos);//.getPtr(), sizeof(float2));
    opencl.kernel("updateParticle")->setArg(3, dimensions);//.getPtr(), sizeof(float2) );
    glFlush();
    
    opencl.kernel("updateParticle")->run1D(NUM_PARTICLES);
    
}

void CinderOpenCLApp::draw()
{
    opencl.finish();
    gl::clear();
    
    gl::color(1.0f, 1.0f, 1.0f);
    
    gl::ScopedVao v( vao );
    gl::context()->setDefaultShaderVars();
    gl::drawArrays( GL_POINTS, 0, NUM_PARTICLES );
    gl::color(1, 1, 1);
    
    
}

CINDER_APP( CinderOpenCLApp, RendererGl )
