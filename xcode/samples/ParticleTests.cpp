#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "CinderOpenCL.h"
#include "cinder/Log.h"
#include "OCLManagedBuffer.h"
#include <OpenCL/OpenCL.h>
#include "cinder/Rand.h"
using namespace ci;
using namespace ci::app;
using namespace std;


#define NUM_PARTICLES (1000000)

struct float2{
    float x,y;
    
    void set(int x,int y){
        this->x = x;
        this->y = y;
    }
};

typedef struct{
    float2 vel;
    float mass;
    float dummy;		// need this to make sure the float2 vel is aligned to a 16 byte boundary
} Particle;


class CinderOpenCLApp : public App {
public:
    void setup() override;
    void mouseMove(MouseEvent event) override;
    void mouseDown( MouseEvent event ) override;
    void update() override;
    void draw() override;
    
    CinderOpenCL cl;
    const char * KernelSource;
    ci::gl::GlslProgRef renderShader;
    ci::vec2			mousePos;
    ci::vec2				dimensions;
    
    OCLManagedBufferT<Particle>particles; // vector of Particles on host and corresponding clBuffer on device
    
    float2 particlesPos[NUM_PARTICLES];
    
    OCLManagedBufferT<float2> particlePos; // vector of particle positions on host and corresponding clBuffer, and vbo on device
    
    
    ci::gl::VboRef vbo;
    
};

void CinderOpenCLApp::setup()
{
    
    renderShader = gl::getStockShader(gl::ShaderDef().color());
    
    //init vbo
    vbo = ci::gl::Vbo::create( GL_ARRAY_BUFFER_ARB, sizeof(float2) * NUM_PARTICLES, 0, GL_DYNAMIC_COPY_ARB );
    
    vbo->bind();
    ci::gl::vertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    gl::enableVertexAttribArray(0);
    vbo->unbind();
    
    
    cl.setupFromOpenGL();
    
    particles.initBuffer(NUM_PARTICLES);
    particlePos.initFromGLObject(vbo->getId(), NUM_PARTICLES);
    
    // init data
    
    for(int i=0; i<NUM_PARTICLES; i++) {
        Particle &p = particles[i];
        p.vel.set(0, 0);
        p.mass = randFloat(0.5, 1);
        particlePos[i].set(randFloat(0, app::getWindowWidth()), randFloat(0,app::getWindowHeight()));
    }
    
    particles.writeToDevice();
    particlePos.writeToDevice(); ///< uploads buffer data to shared CL/GL memory, so the vbo and the cl buffer are written in one go, since they occupy the same memory locations.
    
    
    cl.loadProgram("Particle.cl");
    cl.loadKernel("updateParticle");
    
    cl.kernel("updateParticle")->setArg(0, particles);
    cl.kernel("updateParticle")->setArg(1, particlePos);
    cl.kernel("updateParticle")->setArg(2, mousePos);//.getPtr(), sizeof(float2));
    cl.kernel("updateParticle")->setArg(3, dimensions);//.getPtr(), sizeof(float2));
    
    
    //set dimensions
    dimensions = app::getWindowSize();
    
    gl::pointSize(10);
    
}
void CinderOpenCLApp::mouseMove(cinder::app::MouseEvent event)
{
    mousePos = event.getPos();
}
void CinderOpenCLApp::mouseDown( MouseEvent event )
{
}

void CinderOpenCLApp::update()
{
    cl.kernel("updateParticle")->setArg(2, mousePos);//.getPtr(), sizeof(float2));
    cl.kernel("updateParticle")->setArg(3, dimensions);//.getPtr(), sizeof(float2) );
    glFlush();
    
    cl.kernel("updateParticle")->run1D(NUM_PARTICLES);
    
}

void CinderOpenCLApp::draw()
{
    cl.finish();
    gl::clear( Color( 0, 0, 0 ) );
    gl::ScopedGlslProg render(renderShader);
    gl::ScopedState	stateScope( GL_PROGRAM_POINT_SIZE, true );
    
    gl::color(255, 0, 0);
    cl.finish();
    vbo->bind();
    gl::drawArrays(GL_POINTS, 0, NUM_PARTICLES);
    
}

//CINDER_APP( CinderOpenCLApp, RendererGl )
