#include "MSAOpenCL.h"
#include <OpenCL/Opencl.h>
#include <OpenGL/OpenGL.h>
#include "CLUtilities.h"
#include "MSAOpenCLProgram.h"
//#include "cinder/Log.h"
#include "cinder/Utilities.h"
using namespace ci;
using namespace std;


	OpenCL *OpenCL::currentOpenCL = NULL;


	OpenCL::OpenCL() {
        app::console()<< "OpenCL::OpenCL\n";
		isSetup		= false;
		clContext	= NULL;
		clDevice	= NULL;
		clQueue		= NULL;
	}

	OpenCL::~OpenCL() {
		clFinish(clQueue);

		for(int i=0; i<memObjects.size(); i++) delete memObjects[i];	// FIX
		kernels.clear();
		clearPrograms();

		clReleaseCommandQueue(clQueue);
		clReleaseContext(clContext);
        app::console()<< "OpenCL::~OpenCL\n";
	}
    
    void OpenCL::setup(bool useOpenGL,int clDeviceType,int deviceNumber){
        if (useOpenGL) {
            setupFromOpenGL();
        }else{
            setup(clDeviceType,deviceNumber);
        }
    }


	void OpenCL::setup(int clDeviceType_, int deviceNumber) {
        app::console()<<  "OpenCL::setup " + ci::toString(clDeviceType_)<<"\n";

		if(isSetup) {
            app::console()<< "... already setup. returning\n";
			return;
		}

		if(deviceInfo.size() == 0) getDeviceInfos(clDeviceType_);
		deviceNumber = (deviceNumber + getNumDevices()) % getNumDevices();
		clDevice = deviceInfo[deviceNumber].clDeviceId;

		cl_int err;
		clContext = clCreateContext(NULL, 1, &clDevice, NULL, NULL, &err);
		if(clContext == NULL) {
            app::console()<<  "Error creating clContext.\n";
			assert(err != CL_INVALID_PLATFORM);
			assert(err != CL_INVALID_VALUE);
			assert(err != CL_INVALID_DEVICE);
			assert(err != CL_INVALID_DEVICE_TYPE);
			assert(err != CL_DEVICE_NOT_AVAILABLE);
			assert(err != CL_DEVICE_NOT_FOUND);
			assert(err != CL_OUT_OF_HOST_MEMORY);
			assert(false);
		}


		createQueue();
	}	


	void OpenCL::setupFromOpenGL(int deviceNumber) {
        app::console()<<  "OpenCL::setupFromOpenGL \n";

		if(isSetup) {
            app::console()<< "... already setup. returning\n";
			return;
		}

		getDeviceInfos(CL_DEVICE_TYPE_GPU);
		deviceNumber = (deviceNumber + getNumDevices()) % getNumDevices();
		clDevice = deviceInfo[deviceNumber].clDeviceId;
	
		cl_int err;

		CGLContextObj kCGLContext = CGLGetCurrentContext();
		CGLShareGroupObj kCGLShareGroup = CGLGetShareGroup(kCGLContext);
		cl_context_properties properties[] = { CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE, (cl_context_properties)kCGLShareGroup, 0 };

		clContext = clCreateContext(properties, 0, 0, NULL, NULL, &err);

		if(clContext == NULL) {
            app::console()<< "Error creating clContext.\n";
			assert(err != CL_INVALID_PLATFORM);
			assert(err != CL_INVALID_VALUE);
			assert(err != CL_INVALID_DEVICE);
			assert(err != CL_INVALID_DEVICE_TYPE);
			assert(err != CL_DEVICE_NOT_AVAILABLE);
			assert(err != CL_DEVICE_NOT_FOUND);
			assert(err != CL_OUT_OF_HOST_MEMORY);
			assert(false);
        }else{
            app::console()<<"CONTEXT MADE!\n";
        }
        
		createQueue();
	}	


	cl_device_id& OpenCL::getDevice() {
		return clDevice;
	}


	cl_context& OpenCL::getContext() {
		return clContext;
	}

	cl_command_queue& OpenCL::getQueue() {
		return clQueue;
	}



	OpenCLProgramPtr  OpenCL::loadProgramFromFile(string filename, bool isBinary) {
		app::console()<< "OpenCL::loadProgramFromFile\n";
		OpenCLProgramPtr p = OpenCLProgramPtr (new OpenCLProgram());
        p->loadFromFile(filename);
		programs[filename] = p;
		return p;
	}


	OpenCLKernelPtr OpenCL::loadKernel(string kernelName, OpenCLProgramPtr program) {
		//CI_LOG_I( "OpenCL::loadKernel " + kernelName + ", " + ofToString((int)program.get()));
		if(program.get() == NULL) program = (programs.begin()->second);
		OpenCLKernelPtr k = program->loadKernel(kernelName);
		kernels[kernelName] = k;
		return k;
	}


	OpenCLBuffer* OpenCL::createBuffer(int numberOfBytes, cl_mem_flags memFlags, void *dataPtr, bool blockingWrite) {
		OpenCLBuffer *clBuffer = new OpenCLBuffer();
		clBuffer->initBuffer(numberOfBytes, memFlags, dataPtr);
		memObjects.push_back(clBuffer);
		return clBuffer;
	}


	OpenCLBuffer* OpenCL::createBufferFromGLObject(GLuint glBufferObject, cl_mem_flags memFlags) {
		OpenCLBuffer *clBuffer = new OpenCLBuffer();
		clBuffer->initFromGLObject(glBufferObject, memFlags);
		memObjects.push_back(clBuffer);
		return clBuffer;
	}


	/*
     OpenCLImage* OpenCL::createImage2D(int width, int height, cl_channel_order imageChannelOrder, cl_channel_type imageChannelDataType, cl_mem_flags memFlags, void *dataPtr, bool blockingWrite) {
     return createImage3D(width, height, 1, imageChannelOrder, imageChannelDataType, memFlags, dataPtr, blockingWrite);
     }
     
     
     OpenCLImage* OpenCL::createImageFromTexture(ofTexture &tex, cl_mem_flags memFlags, int mipLevel) {
     OpenCLImage *clImage = new OpenCLImage();
     clImage->initFromTexture(tex, memFlags, mipLevel);
     memObjects.push_back(clImage);
     return clImage;
     }
     
     OpenCLImage* OpenCL::createImageWithTexture(int width, int height, int glType, cl_mem_flags memFlags) {
     OpenCLImage *clImage = new OpenCLImage();
     clImage->initWithTexture(width, height, glType, memFlags);
     memObjects.push_back(clImage);
     return clImage;
     }
     
     
     OpenCLImage* OpenCL::createImage3D(int width, int height, int depth, cl_channel_order imageChannelOrder, cl_channel_type imageChannelDataType, cl_mem_flags memFlags, void *dataPtr, bool blockingWrite) {
     OpenCLImage *clImage = new OpenCLImage();
     clImage->initWithoutTexture(width, height, depth, imageChannelOrder, imageChannelDataType, memFlags, dataPtr, blockingWrite);
     memObjects.push_back(clImage);
     return clImage;
     }
*/


	OpenCLKernelPtr OpenCL::kernel(string kernelName) {
		// todo: check if kernel could be found.
		if (kernels.find(kernelName) != kernels.end()) {
			return kernels[kernelName];
		} else {
			app::console() << "Could not find kernel with name: " << kernelName<<"\n";
			return OpenCLKernelPtr();	// return empty shared ptr.
		}
	}

	void OpenCL::flush() {
		clFlush(clQueue);
	}


	void OpenCL::finish() {
		clFinish(clQueue);
	}



	int OpenCL::getDeviceInfos(int clDeviceType) {
		cl_int err;

		cl_uint numPlatforms=0;
		err = clGetPlatformIDs( NULL, NULL, &numPlatforms ); ///< first, only fetch number of platforms.
		vector<cl_platform_id> platformIdBuffer;
		platformIdBuffer.resize(numPlatforms); /// resize to correct size

		//	windows AMD sdk/ati radeon driver implementation doesn't accept NULL as a platform ID, so fetch it first
		err = clGetPlatformIDs(	numPlatforms, platformIdBuffer.data(), NULL);
		platformIdBuffer.resize(numPlatforms);

		//	error fetching platforms... try NULL anyway
		if ( err != CL_SUCCESS || numPlatforms == 0 )
		{
			platformIdBuffer[0] = NULL;
			numPlatforms = 1;
		}

		/// a map over all platforms and devices.
		map<cl_platform_id, vector<cl_device_id> > devicesPerPlatform;

		int totalDevicesFound = 0;
		//	find first successfull platform
		// TODO: what if there is more than one platform?
		for ( int p=0;	p < numPlatforms;	p++ ) {
			cl_platform_id platformId = platformIdBuffer[p];

			// first only retrieve the number of devices.
			cl_uint numDevices=0;
			err = clGetDeviceIDs(platformId, clDeviceType, NULL, NULL, &numDevices);
			if ( err == CL_SUCCESS ) {
				//--------! invariant: numDevices now holds the numer of devices in this particular platform.
				vector<cl_device_id> deviceIds;
				deviceIds.resize(numDevices); // make sure there's enough space in there.
				/// now retrieve any device ids from the OpenCL platform into the deviceIds vector.
				err = clGetDeviceIDs(platformId, clDeviceType, numDevices, deviceIds.data(), NULL);
				if ( err == CL_SUCCESS ) {
					/// now store all found devices into the map.
					devicesPerPlatform[platformId] = deviceIds;
					totalDevicesFound += deviceIds.size();
				}
			}
		}

		// reset err.
		err = 0;

        app::console()<< ci::toString(totalDevicesFound) + " devices found, on " + std::to_string(numPlatforms) + " platforms\n";

		//	no platforms worked
		if ( totalDevicesFound == 0) {
            app::console()<< "Error finding clDevices.\n";
			assert(false);
			return 0;
		}	


		deviceInfo.clear();

		// now we map over all platforms and devices and collect all data we need.

		for ( map<cl_platform_id, vector<cl_device_id> >::iterator it = devicesPerPlatform.begin(); it != devicesPerPlatform.end(); it++)
		{
			vector<cl_device_id>& devices = it->second;

			for (int i = 0; i<devices.size(); i++){ 
				DeviceInfo info;

				info.clDeviceId = devices[i];  ///< store deviceID
				info.clPlatformId = it->first; ///< store platformID with device

				err = clGetDeviceInfo(info.clDeviceId, CL_DEVICE_VENDOR, sizeof(info.vendorName), info.vendorName, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_NAME, sizeof(info.deviceName), info.deviceName, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DRIVER_VERSION, sizeof(info.driverVersion), info.driverVersion, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_VERSION, sizeof(info.deviceVersion), info.deviceVersion, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(info.maxComputeUnits), &info.maxComputeUnits, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(info.maxWorkItemDimensions), &info.maxWorkItemDimensions, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(info.maxWorkItemSizes), &info.maxWorkItemSizes, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(info.maxWorkGroupSize), &info.maxWorkGroupSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(info.maxClockFrequency), &info.maxClockFrequency, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(info.maxMemAllocSize), &info.maxMemAllocSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE_SUPPORT, sizeof(info.imageSupport), &info.imageSupport, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(info.maxReadImageArgs), &info.maxReadImageArgs, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(info.maxWriteImageArgs), &info.maxWriteImageArgs, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(info.image2dMaxWidth), &info.image2dMaxWidth, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(info.image2dMaxHeight), &info.image2dMaxHeight, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(info.image3dMaxWidth), &info.image3dMaxWidth, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(info.image3dMaxHeight), &info.image3dMaxHeight, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(info.image3dMaxDepth), &info.image3dMaxDepth, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_SAMPLERS, sizeof(info.maxSamplers), &info.maxSamplers, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_PARAMETER_SIZE, sizeof(info.maxParameterSize), &info.maxParameterSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(info.globalMemCacheSize), &info.globalMemCacheSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(info.globalMemSize), &info.globalMemSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(info.maxConstantBufferSize), &info.maxConstantBufferSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(info.maxConstantArgs), &info.maxConstantArgs, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(info.localMemSize), &info.localMemSize, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(info.errorCorrectionSupport), &info.errorCorrectionSupport, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_PROFILING_TIMER_RESOLUTION, sizeof(info.profilingTimerResolution), &info.profilingTimerResolution, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_ENDIAN_LITTLE, sizeof(info.endianLittle), &info.endianLittle, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_PROFILE, sizeof(info.profile), info.profile, NULL);
				err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_EXTENSIONS, sizeof(info.extensions), info.extensions, NULL);

				deviceInfo.push_back(info);

				if(err != CL_SUCCESS) {
                    app::console()<< "Error getting clDevice information.\n";
                    CI_LOG_E(CLUtilities::getErrorMessage(err));
					assert(false);
				}

                app::console()<< getInfoAsString(i) << "\n";
			}
		}

		return deviceInfo.size();
	}

	string OpenCL::getInfoAsString(int deviceNumber) {
		deviceNumber = (deviceNumber + getNumDevices()) % getNumDevices();
		DeviceInfo &info = deviceInfo[deviceNumber];
		return "";
	}


	void OpenCL::createQueue() {
		int err = 0;
		clQueue = clCreateCommandQueue(clContext, clDevice, 0, &err);
		if(clQueue == NULL || err != CL_SUCCESS ) {
            app::console()<< "Error creating command queue.\n";

		}

		isSetup = true;
		currentOpenCL = this;
	}


