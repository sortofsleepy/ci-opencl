#pragma once

#include <OpenCL/Opencl.h>
#include "MSAOpenCLTypes.h"
#include "cinder/Log.h"

    
    class OpenCLProgram {
    public:
        OpenCLProgram();
        ~OpenCLProgram();
        
        void loadFromFile(string filename, bool isBinary = false);
        void loadFromSource(string source);
        
        OpenCLKernelPtr loadKernel(string kernelName);
        
        void getBinary();
        
        cl_program& getCLProgram();
        
    protected:
        OpenCL*		pOpenCL;
        cl_program		clProgram;
        
        void			build();
        
    };
    
