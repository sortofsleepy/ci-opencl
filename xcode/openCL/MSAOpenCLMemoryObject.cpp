#include "MSAOpenCL.h"
#include "MSAOpenCLMemoryObject.h"
#include "CLUtilities.h"
#include "cinder/Log.h"

using namespace ci;
using namespace std;

	
	OpenCLMemoryObject::OpenCLMemoryObject()
	: pOpenCL(NULL)
	, clMemObject(NULL)
	, hasCorrespondingGLObject(false)
	, hasGLObjectOwnership(false)
	{
	}
	
	// ----------------------------------------------------------------------
	
	OpenCLMemoryObject::~OpenCLMemoryObject() {
        app::console()<< "OpenCLMemoryObject::~OpenCLMemoryObject\n";
		if(clMemObject) clReleaseMemObject(clMemObject);
	}
	
	// ----------------------------------------------------------------------
	
	void OpenCLMemoryObject::memoryObjectInit() {
        app::console()<< "OpenCLMemoryObject::memoryObjectInit\n";
		pOpenCL = OpenCL::currentOpenCL;
	}
	
	// ----------------------------------------------------------------------
	
	bool OpenCLMemoryObject::lockGLObject(){

		/// we'll always return true if there's no OpenGL object to lock.
		if (!hasCorrespondingGLObject) return true;
		
		
		if (hasGLObjectOwnership) {
            app::console() << "Attempt to lock same openGL object twice for OpenCL. OpenCL already has ownership.\n";
			return false;
		}
		
			cl_int err = clEnqueueAcquireGLObjects(pOpenCL->getQueue(), 1 , &clMemObject, 0, NULL, NULL);

		if (err != CL_SUCCESS){
            app::console() << "could not acquire gl object\n";
            CI_LOG_E(CLUtilities::getErrorMessage(err));
			return false;
		}
		//----------| invariant: we were successful in acquiring our object.
		hasGLObjectOwnership = true;
		return true;
	}
	
	// ----------------------------------------------------------------------
	
	bool OpenCLMemoryObject::unlockGLObject(){
		// we'll always return true if there's no OpenGL object to unlock.
		if (!hasCorrespondingGLObject) return true;
		
		
		if ( !hasGLObjectOwnership) {
            app::console() << "Attempt to unlock openGL object that was not in OpenCL ownership.\n";
			return false;
		}
		
		cl_int err = clEnqueueReleaseGLObjects(pOpenCL->getQueue(), 1 , &clMemObject, 0, NULL, NULL);
		
		if (err != CL_SUCCESS){
			app::console() << "could not acquire gl object\n";
            CI_LOG_E(CLUtilities::getErrorMessage(err));
			return false;
		}
		//----------| invariant: we were successful in releasing our object.
		hasGLObjectOwnership = false;
		return true;
	}

