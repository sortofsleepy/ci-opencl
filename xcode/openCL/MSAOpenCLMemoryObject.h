/***********************************************************************
 
 OpenCL Memory Object base class for Images and Buffers
 Do not instantiate this class
 
 ************************************************************************/

#pragma once

#include <OpenCL/Opencl.h>


	class OpenCL;
	
	class OpenCLMemoryObject {
		friend class OpenCLKernel;
		
	public:
		virtual ~OpenCLMemoryObject();
		
        cl_mem& getCLMem() { return clMemObject; }
		operator cl_mem&() { return getCLMem(); }
	
		/// Takes ownership of corresponding OpenGL object, if any
		bool lockGLObject();
		/// Releases ownership of corresponding OpenGL object, if any
		bool unlockGLObject();
	
	protected:
		OpenCLMemoryObject();

		cl_mem		clMemObject;
		OpenCL*		pOpenCL;
		
		void memoryObjectInit();

		bool hasCorrespondingGLObject;
		bool hasGLObjectOwnership;
		
	};

