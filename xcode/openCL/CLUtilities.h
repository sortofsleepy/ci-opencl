//
//  CLUtilities.h
//  CinderOpenCL
//
//  Created by Joseph Chow on 12/1/15.
//
//

#pragma once
#ifndef CinderOpenCL_CLUtilities_h
#define CinderOpenCL_CLUtilities_h
#include <OpenCL/OpenCL.h>
#include "cinder/Utilities.h"
#include "cinder/Log.h"
using namespace ci;
using namespace ci::app;
using namespace std;

struct DeviceInfo {
    cl_platform_id clPlatformId; /// < platform this device belongs to
    cl_device_id clDeviceId;
    cl_char		vendorName[1024];
    cl_char		deviceName[1024];
    cl_char		driverVersion[1024];
    cl_char		deviceVersion[1024];
    cl_uint		maxComputeUnits;
    cl_uint		maxWorkItemDimensions;
    size_t		maxWorkItemSizes[32];
    size_t		maxWorkGroupSize;
    cl_uint		maxClockFrequency;
    cl_ulong	maxMemAllocSize;
    cl_bool		imageSupport;
    cl_uint		maxReadImageArgs;
    cl_uint		maxWriteImageArgs;
    size_t		image2dMaxWidth;
    size_t		image2dMaxHeight;
    size_t		image3dMaxWidth;
    size_t		image3dMaxHeight;
    size_t		image3dMaxDepth;
    cl_uint		maxSamplers;
    size_t		maxParameterSize;
    cl_ulong	globalMemCacheSize;
    cl_ulong	globalMemSize;
    cl_ulong	maxConstantBufferSize;
    cl_uint		maxConstantArgs;
    cl_ulong	localMemSize;
    cl_bool		errorCorrectionSupport;
    size_t		profilingTimerResolution;
    cl_bool		endianLittle;
    cl_char		profile[1024];
    cl_char		extensions[1024];
};


class CLUtilities {
  
    std::vector<DeviceInfo> deviceInfo;
public:
    CLUtilities(){}
    
    void getDevices(int clDeviceType=CL_DEVICE_TYPE_GPU){
        cl_int err;
        
        cl_uint numPlatforms=0;
        err = clGetPlatformIDs( NULL, NULL, &numPlatforms ); ///< first, only fetch number of platforms.
        vector<cl_platform_id> platformIdBuffer;
        platformIdBuffer.resize(numPlatforms); /// resize to correct size
        
        //	windows AMD sdk/ati radeon driver implementation doesn't accept NULL as a platform ID, so fetch it first
        err = clGetPlatformIDs(	numPlatforms, platformIdBuffer.data(), NULL);
        platformIdBuffer.resize(numPlatforms);
        
        //	error fetching platforms... try NULL anyway
        if ( err != CL_SUCCESS || numPlatforms == 0 )
        {
            platformIdBuffer[0] = NULL;
            numPlatforms = 1;
        }
        
        /// a map over all platforms and devices.
        map<cl_platform_id, vector<cl_device_id> > devicesPerPlatform;
        
        int totalDevicesFound = 0;
        //	find first successfull platform
        // TODO: what if there is more than one platform?
        for ( int p=0;	p < numPlatforms;	p++ ) {
            cl_platform_id platformId = platformIdBuffer[p];
            
            // first only retrieve the number of devices.
            cl_uint numDevices=0;
            err = clGetDeviceIDs(platformId, clDeviceType, NULL, NULL, &numDevices);
            if ( err == CL_SUCCESS ) {
                //--------! invariant: numDevices now holds the numer of devices in this particular platform.
                vector<cl_device_id> deviceIds;
                deviceIds.resize(numDevices); // make sure there's enough space in there.
                /// now retrieve any device ids from the OpenCL platform into the deviceIds vector.
                err = clGetDeviceIDs(platformId, clDeviceType, numDevices, deviceIds.data(), NULL);
                if ( err == CL_SUCCESS ) {
                    /// now store all found devices into the map.
                    devicesPerPlatform[platformId] = deviceIds;
                    totalDevicesFound += deviceIds.size();
                }
            }
        }
        
        // reset err.
        err = 0;
        
        app::console()<< ci::toString(totalDevicesFound) + " devices found, on " + ci::toString(numPlatforms) + " platforms\n";
        
        //	no platforms worked
        if ( totalDevicesFound == 0) {
            app::console()<< "Error finding clDevices.\n";
            assert(false);
        }
        
        
        deviceInfo.clear();
        
        // now we map over all platforms and devices and collect all data we need.
        
        for ( map<cl_platform_id, vector<cl_device_id> >::iterator it = devicesPerPlatform.begin(); it != devicesPerPlatform.end(); it++)
        {
            vector<cl_device_id>& devices = it->second;
            
            for (int i = 0; i<devices.size(); i++){
                DeviceInfo info;
                
                info.clDeviceId = devices[i];  ///< store deviceID
                info.clPlatformId = it->first; ///< store platformID with device
                
                err = clGetDeviceInfo(info.clDeviceId, CL_DEVICE_VENDOR, sizeof(info.vendorName), info.vendorName, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_NAME, sizeof(info.deviceName), info.deviceName, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DRIVER_VERSION, sizeof(info.driverVersion), info.driverVersion, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_VERSION, sizeof(info.deviceVersion), info.deviceVersion, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(info.maxComputeUnits), &info.maxComputeUnits, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(info.maxWorkItemDimensions), &info.maxWorkItemDimensions, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(info.maxWorkItemSizes), &info.maxWorkItemSizes, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(info.maxWorkGroupSize), &info.maxWorkGroupSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(info.maxClockFrequency), &info.maxClockFrequency, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(info.maxMemAllocSize), &info.maxMemAllocSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE_SUPPORT, sizeof(info.imageSupport), &info.imageSupport, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(info.maxReadImageArgs), &info.maxReadImageArgs, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(info.maxWriteImageArgs), &info.maxWriteImageArgs, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(info.image2dMaxWidth), &info.image2dMaxWidth, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(info.image2dMaxHeight), &info.image2dMaxHeight, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(info.image3dMaxWidth), &info.image3dMaxWidth, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(info.image3dMaxHeight), &info.image3dMaxHeight, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(info.image3dMaxDepth), &info.image3dMaxDepth, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_SAMPLERS, sizeof(info.maxSamplers), &info.maxSamplers, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_PARAMETER_SIZE, sizeof(info.maxParameterSize), &info.maxParameterSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(info.globalMemCacheSize), &info.globalMemCacheSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(info.globalMemSize), &info.globalMemSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(info.maxConstantBufferSize), &info.maxConstantBufferSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(info.maxConstantArgs), &info.maxConstantArgs, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(info.localMemSize), &info.localMemSize, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(info.errorCorrectionSupport), &info.errorCorrectionSupport, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_PROFILING_TIMER_RESOLUTION, sizeof(info.profilingTimerResolution), &info.profilingTimerResolution, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_ENDIAN_LITTLE, sizeof(info.endianLittle), &info.endianLittle, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_PROFILE, sizeof(info.profile), info.profile, NULL);
                err |= clGetDeviceInfo(info.clDeviceId, CL_DEVICE_EXTENSIONS, sizeof(info.extensions), info.extensions, NULL);
                
                deviceInfo.push_back(info);
                
                if(err != CL_SUCCESS) {
                    app::console()<< "Error getting clDevice information.\n";
                    assert(false);
                }
                
                app::console()<< getInfoAsString(i) << "\n";
            }
        }
        
    }
    
    
    //! Returns the device information as a string
    string getInfoAsString(int deviceNumber) {
        
        deviceNumber = (deviceNumber + deviceInfo.size()) % deviceInfo.size();
        
        DeviceInfo &info = deviceInfo[deviceNumber];
        return string("\n\n*********\nOpenCL Device information for device #") + ci::toString(deviceNumber) +
        "\n cl_device_id................" + ci::toString(info.clDeviceId) +
        "\n vendorName.................." + string((char*)info.vendorName) +
        "\n deviceName.................." + string((char*)info.deviceName) +
        "\n driverVersion..............." + string((char*)info.driverVersion) +
        "\n deviceVersion..............." + string((char*)info.deviceVersion) +
        "\n maxComputeUnits............." + ci::toString(info.maxComputeUnits)+
        "\n maxWorkItemDimensions......." + ci::toString(info.maxWorkItemDimensions) +
        "\n maxWorkItemSizes[0]........." + ci::toString(info.maxWorkItemSizes[0]) +
        "\n maxWorkGroupSize............" + ci::toString(info.maxWorkGroupSize) +
        "\n maxClockFrequency..........." + ci::toString(info.maxClockFrequency) +
        "\n maxMemAllocSize............." + ci::toString(info.maxMemAllocSize/1024.0f/1024.0f) + " MB" +
        "\n imageSupport................" + (info.imageSupport ? "YES" : "NO") +
        "\n maxReadImageArgs............" + ci::toString(info.maxReadImageArgs) +
        "\n maxWriteImageArgs..........." + ci::toString(info.maxWriteImageArgs) +
        "\n image2dMaxWidth............." + ci::toString(info.image2dMaxWidth) +
        "\n image2dMaxHeight............" + ci::toString(info.image2dMaxHeight) +
        "\n image3dMaxWidth............." + ci::toString(info.image3dMaxWidth) +
        "\n image3dMaxHeight............" + ci::toString(info.image3dMaxHeight) +
        "\n image3dMaxDepth............." + ci::toString(info.image3dMaxDepth) +
        "\n maxSamplers................." + ci::toString(info.maxSamplers) +
        "\n maxParameterSize............" + ci::toString(info.maxParameterSize) +
        "\n globalMemCacheSize.........." + ci::toString(info.globalMemCacheSize/1024.0f/1024.0f) + " MB" +
        "\n globalMemSize..............." + ci::toString(info.globalMemSize/1024.0f/1024.0f) + " MB" +
        "\n maxConstantBufferSize......." + ci::toString(info.maxConstantBufferSize/1024.0f) + " KB"
        "\n maxConstantArgs............." + ci::toString(info.maxConstantArgs) +
        "\n localMemSize................" + ci::toString(info.localMemSize/1024.0f) + " KB"
        "\n errorCorrectionSupport......" + (info.errorCorrectionSupport ? "YES" : "NO") +
        "\n profilingTimerResolution...." + ci::toString(info.profilingTimerResolution) +
        "\n endianLittle................" + ci::toString(info.endianLittle) +
        "\n profile....................." + string((char*)info.profile) +
        "\n extensions.................." + string((char*)info.extensions) +
        "\n*********\n\n";
    }
    
    /**
     *  Simple error interpreter to interpret codes.
     */
    static string getErrorMessage(cl_int err){
        switch (err) {
            case CL_SUCCESS:                            return "Success!";
            case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
            case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
            case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
            case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
            case CL_OUT_OF_RESOURCES:                   return "Out of resources";
            case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
            case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
            case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
            case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
            case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
            case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
            case CL_MAP_FAILURE:                        return "Map failure";
            case CL_INVALID_VALUE:                      return "Invalid value";
            case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
            case CL_INVALID_PLATFORM:                   return "Invalid platform";
            case CL_INVALID_DEVICE:                     return "Invalid device";
            case CL_INVALID_CONTEXT:                    return "Invalid context";
            case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
            case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
            case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
            case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
            case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
            case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
            case CL_INVALID_SAMPLER:                    return "Invalid sampler";
            case CL_INVALID_BINARY:                     return "Invalid binary";
            case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
            case CL_INVALID_PROGRAM:                    return "Invalid program";
            case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
            case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
            case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
            case CL_INVALID_KERNEL:                     return "Invalid kernel";
            case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
            case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
            case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
            case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
            case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
            case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
            case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
            case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
            case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
            case CL_INVALID_EVENT:                      return "Invalid event";
            case CL_INVALID_OPERATION:                  return "Invalid operation";
            case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
            case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
            case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
            default: return "Unknown";
        }
    }

};
#endif
