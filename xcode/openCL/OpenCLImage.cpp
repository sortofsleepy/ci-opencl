#include "MSAOpenCL.h"
#include "OpenCLImage.h"
using namespace ci;
using namespace std;

    OpenCLImage::OpenCLImage() {
        app::console()<< "OpenCLImage::OpenCLImage\n";
        texture = NULL;
    }
    
    
    void OpenCLImage::initWithoutTexture(int w,
                                         int h,
                                         int d,
                                         cl_channel_order imageChannelOrder,
                                         cl_channel_type imageChannelDataType,
                                         cl_mem_flags memFlags,
                                         void *dataPtr,
                                         bool blockingWrite)
    {
        app::console()<< "OpenCLImage::initWithoutTexture\n";
        
        init(w, h, d);
        
        cl_int err;
        cl_image_format imageFormat;
        imageFormat.image_channel_order		= imageChannelOrder;
        imageFormat.image_channel_data_type	= imageChannelDataType;
        

     //   int image_row_pitch = 0;	// TODO
      //  int image_slice_pitch = 0;
        
        if(clMemObject) clReleaseMemObject(clMemObject);
        
        if(depth == 1) {
            cl_image_desc desc;
            desc.image_width = w;
            desc.image_height = h;
            desc.image_depth = depth;
            desc.image_type = CL_MEM_OBJECT_IMAGE2D;
            
            clMemObject = clCreateImage(pOpenCL->getContext(), CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, &imageFormat, &desc, dataPtr, &err);
        } else {
            cl_image_desc desc;
            desc.image_width = w;
            desc.image_depth = depth;
            
            desc.image_height = h;
            desc.image_type = CL_MEM_OBJECT_IMAGE3D;
            
            clCreateImage(pOpenCL->getContext(), CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, &imageFormat, &desc, dataPtr, &err);
        }
        assert(err != CL_INVALID_CONTEXT);
        assert(err != CL_INVALID_VALUE);
        assert(err != CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
        assert(err != CL_INVALID_IMAGE_SIZE );
        assert(err != CL_INVALID_HOST_PTR);
        assert(err != CL_IMAGE_FORMAT_NOT_SUPPORTED);
        assert(err != CL_MEM_OBJECT_ALLOCATION_FAILURE);
        assert(err != CL_INVALID_OPERATION);
        assert(err != CL_OUT_OF_HOST_MEMORY );
        assert(err == CL_SUCCESS);
        assert(clMemObject);
        
        if(dataPtr) {
            write(dataPtr, blockingWrite);
        }
        
  
        texture = NULL;
    }
    
    
    
void OpenCLImage::initFromTexture(ci::gl::TextureRef tex,
                                      cl_mem_flags memFlags,
                                      int mipLevel)
    {
        
        app::console()<< "OpenCLImage::initFromTexture" << "\n";
        
        init(tex->getWidth(), tex->getHeight(), 1);
        
        cl_int err;
        if(clMemObject) clReleaseMemObject(clMemObject);
        
        
        clMemObject = clCreateFromGLTexture2D(pOpenCL->getContext(), memFlags, tex->getTarget(), mipLevel, tex->getId(), &err);
        assert(err != CL_INVALID_CONTEXT);
        assert(err != CL_INVALID_VALUE);
        //	assert(err != CL_INVALID_MIPLEVEL);
        assert(err != CL_INVALID_GL_OBJECT);
        assert(err != CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
        assert(err != CL_OUT_OF_HOST_MEMORY);
        assert(err == CL_SUCCESS);
        assert(clMemObject);
        
        texture = tex;
        hasCorrespondingGLObject = true;
    }
    
    
    
    
    void OpenCLImage::initWithTexture(int w,
                                      int h,
                                      int glTypeInternal,
                                      cl_mem_flags memFlags)
    {
        app::console() << "OpenCLImage::initWithTexture" << "\n";
        
        gl::Texture::Format fmt;
        fmt.setInternalFormat(glTypeInternal);
        texture = gl::Texture::create(w, h,fmt);
        reset();
    }
    
    
    
    void OpenCLImage::init(int w, int h, int d) {
        app::console()<< "OpenCLImage::init()\n";
        
        if(d<0) d = 1;
        
        this->width			= w;
        this->height		= h;
        this->depth			= d;
        
        origin[0] = 0;
        origin[1] = 0;
        origin[2] = 0;
        
        region[0] = width;
        region[1] = height;
        region[2] = depth;
        
        app::console()<< "OpenCLImage::init " + ci::toString(width) + ", " + ci::toString(height) + ", " + ci::toString(depth) << "\n";
        
        memoryObjectInit();
    }
    
    void OpenCLImage::reset() {
        app::console()<< "OpenCLImage::reset()\n";
        int numElements = width * height * 4; // TODO, make real
        //if(ofGetGlTypeFromInternal(texture->getTextureData().glInternalFormat == GL_FLOAT)) numElements *= sizeof(cl_float);
        char *data = new char[numElements];
        memset(data, 0, numElements);
        write(data, true);
        delete []data;
    }
    
    void OpenCLImage::read(void *dataPtr, bool blockingRead, size_t *pOrigin, size_t *pRegion, size_t rowPitch, size_t slicePitch) {
        if(pOrigin == NULL) pOrigin = origin;
        if(pRegion == NULL) pRegion = region;
        if (hasCorrespondingGLObject) lockGLObject();
        cl_int err = clEnqueueReadImage(pOpenCL->getQueue(), clMemObject, blockingRead, pOrigin, pRegion, rowPitch, slicePitch, dataPtr, 0, NULL, NULL);
        if (hasCorrespondingGLObject) unlockGLObject();
        assert(err == CL_SUCCESS);
    }
    
    
    void OpenCLImage::write(void *dataPtr, bool blockingWrite, size_t *pOrigin, size_t *pRegion, size_t rowPitch, size_t slicePitch) {
        if(pOrigin == NULL) pOrigin = origin;
        if(pRegion == NULL) pRegion = region;
        if (hasCorrespondingGLObject) lockGLObject();
        cl_int err = clEnqueueWriteImage(pOpenCL->getQueue(), clMemObject, blockingWrite, pOrigin, pRegion, rowPitch, slicePitch, dataPtr, 0, NULL, NULL);
        if (hasCorrespondingGLObject) unlockGLObject();
        assert(err == CL_SUCCESS);
    }
    
    void OpenCLImage::copyFrom(OpenCLImage &srcImage, size_t *pSrcOrigin, size_t *pDstOrigin, size_t *pRegion) {
        if(pSrcOrigin == NULL) pSrcOrigin = origin;
        if(pDstOrigin == NULL) pDstOrigin = origin;
        if(pRegion == NULL) pRegion = region;
        if (hasCorrespondingGLObject) lockGLObject();
        cl_int err = clEnqueueCopyImage(pOpenCL->getQueue(), srcImage.getCLMem(), clMemObject, pSrcOrigin, pDstOrigin, pRegion, 0, NULL, NULL);
        if (hasCorrespondingGLObject) unlockGLObject();
        assert(err == CL_SUCCESS);
    }
    
    
    
    ci::gl::TextureRef &OpenCLImage::getTexture() {
        return texture;
    }
    
    
    void OpenCLImage::draw(float x, float y) {
        //if(texture) texture->draw(x, y);
        if(texture){
            gl::draw(texture);
        }
    }
    
    void OpenCLImage::draw(float x, float y, float w, float h) {
        //if(texture) texture->draw(x, y, w, h);
        if(texture){
            gl::draw(texture,Rectf(x,y,w,h));
        }
    }


